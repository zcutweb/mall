export const GETGOODSLIST = 'GETGOODSLIST'  //获取商品列表
export const GETPUSHGOODS = 'GETPUSHGOODS' //加载更多商品
export const CHANGEISTOP = 'CHANGEISTOP' //改变istop
export const CHANGESORTTYPE = 'CHANGESORTTYPE' //改变sort
export const CHANGECATEID = 'CHANGECATEID'   //改变cateid
export const CHANGEPAGESIZE = 'CHANGEPAGESIZE'  //改变分页大小
export const CHANGEPRICE = 'CHANGEPRICE'  //改变价格
export const OVERLOAD = 'OVERLOAD'   //重载